# company_app

## Скопируйте код проекта

```
cd existing_repo
git remote add origin https://gitlab.com/AxlAmat/company_app.git
git branch -M main
git push -uf origin main
```

## Перейдите в директорию с кодом проекта

```
cd <путь к директории>/company_app
```

## Запустите compose

```
docker-compose up -d
```
