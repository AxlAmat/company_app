from django_filters.rest_framework import DjangoFilterBackend
from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response

from departments.models import Department, Employee
from departments.serializers import (
    DepartmentSerializer,
    EmployeeResponseSerializer,
    UserRegistrationRequestSerializer,
    UserRegistrationResponseSerializer,
)
from departments.utils import get_tokens, register_user, auth_user


class DepartmentViewSet(viewsets.ModelViewSet):
    queryset = Department.objects.all()
    serializer_class = DepartmentSerializer
    pagination_class = None

    def get_permissions(self):
        if self.action in ("list", "retrieve", "register", "auth"):
            return [AllowAny()]
        return [IsAuthenticated()]

    @swagger_auto_schema(
        request_body=UserRegistrationRequestSerializer,
        responses={200: UserRegistrationResponseSerializer},
    )
    @action(methods=["post"], detail=False)
    def register(self, request: Request) -> Response:
        serializer = UserRegistrationRequestSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = register_user(**serializer.data)
        return Response(data=get_tokens(user))

    @swagger_auto_schema(
        request_body=UserRegistrationRequestSerializer,
        responses={200: UserRegistrationResponseSerializer},
    )
    @action(methods=["post"], detail=False)
    def auth(self, request: Request) -> Response:
        serializer = UserRegistrationRequestSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(data=auth_user(**serializer.data))


class EmployeeViewSet(viewsets.ModelViewSet):
    queryset = Employee.objects.all()
    serializer_class = EmployeeResponseSerializer
    permission_classes = [IsAuthenticated]
    pagination_class = PageNumberPagination
    filter_backends = [DjangoFilterBackend]
    filterset_fields = {
        "last_name": ["icontains", "startswith"],
        "department": ["exact"],
    }
