from django.db import models


class Department(models.Model):
    name = models.CharField("название", max_length=50)

    class Meta:
        verbose_name = "отдел"
        verbose_name_plural = "отделы"

    def __str__(self) -> str:
        return self.name

    def staff_count(self):
        return self.staff.count()

    def salary_avg(self):
        salary = self.staff.aggregate(models.Avg("salary"))
        return salary.get("salary__avg") or 0

    staff_count.short_description = "штат"
    salary_avg.short_description = "средний оклад"


class Employee(models.Model):
    first_name = models.CharField("имя", max_length=20)
    second_name = models.CharField("отчество", max_length=20, blank=True)
    last_name = models.CharField("фамилия", max_length=20, db_index=True)
    photo = models.ImageField("фото", upload_to="photo", blank=True, null=True)
    position = models.CharField("должность", max_length=50)
    salary = models.PositiveIntegerField("оклад")
    age = models.PositiveSmallIntegerField("возраст")
    department = models.ForeignKey(
        Department, on_delete=models.CASCADE, related_name="staff"
    )

    class Meta:
        verbose_name = "сотрудник"
        verbose_name_plural = "сотрудники"
        indexes = [
            models.Index(fields=["last_name"]),
        ]

    def __str__(self) -> str:
        return f"{self.last_name} {self.first_name}"
