from rest_framework import routers

from departments.views import DepartmentViewSet, EmployeeViewSet

router = routers.DefaultRouter()
router.register(r"deps", DepartmentViewSet, basename="department")
router.register(r"empl", EmployeeViewSet, basename="employee")

urlpatterns = router.urls
