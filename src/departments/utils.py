from django.contrib.auth import authenticate, password_validation
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError as DjangoValidationError
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.exceptions import ValidationError as DRFValidationError
from rest_framework_simplejwt.tokens import RefreshToken


EMAIL_REGEXP = (
    r"([A-Za-z0-9]+[.\-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z]{2,})+"
)


def password_validate(password: str) -> str:
    """
    Перехватываем джанговское исключение ValidationError и
    переупаковываем в исключение DRF ValidationError.
    """
    try:
        password_validation.validate_password(password)
    except DjangoValidationError as e:
        raise DRFValidationError({"password": [e.messages]})
    return password


def register_user(email: str, password: str):
    if User.objects.filter(username=email).exists():
        raise AuthenticationFailed(
            "Пользователь с таким адресом уже зарегистрирован."
        )
    return User.objects.create_user(email, email, password)


def get_tokens(user: User) -> dict:
    refresh = RefreshToken().for_user(user)
    return {"access": str(refresh.access_token), "refresh": str(refresh)}


def auth_user(email: str, password: str) -> User:
    user = authenticate(username=email, password=password)
    if not user:
        raise AuthenticationFailed("Не прокатило :(")
    return get_tokens(user)
