import re

from django.core.exceptions import ValidationError
from rest_framework import serializers

from departments.models import Department, Employee
from departments.utils import EMAIL_REGEXP, password_validate


class DepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = ["id", "name", "staff_count", "salary_avg"]


class UserRegistrationRequestSerializer(serializers.Serializer):
    email = serializers.CharField(help_text="почта")
    password = serializers.CharField(help_text="пароль")

    @staticmethod
    def validate_email(value: str) -> str:
        rex = re.compile(EMAIL_REGEXP)
        if not re.fullmatch(rex, value):
            raise ValidationError("Некорректный адрес почты")
        return value

    @staticmethod
    def validate_password(value: str) -> str:
        return password_validate(value)


class UserRegistrationResponseSerializer(serializers.Serializer):
    access = serializers.CharField(help_text="токен доступа")
    refresh = serializers.CharField(help_text="токен для обновления")


class EmployeeResponseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employee
        fields = [
            "id",
            "first_name",
            "second_name",
            "last_name",
            "salary",
            "age",
            "department",
        ]
