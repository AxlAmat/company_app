FROM python:3.11.3-alpine

RUN apk add --no-cache bash && mkdir /code
ADD requirements.txt /code/
RUN pip install -r /code/requirements.txt
ADD src /code/src
WORKDIR /code/src